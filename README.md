Terraform script to create VPC, EC2, ELB(classic),ASG in AWS

The terraform commands to provision the resources are:  
1.terraform init -backend-config="backup.config"
2.terraform plan -var-file="variable_values.tfvars" 
3.terraform apply tfplan

To destroy the resources,use the following command
4.terraform destroy -var-file="variable_values.tfvars"

![picture](img/vpc_ec2.jpg)
