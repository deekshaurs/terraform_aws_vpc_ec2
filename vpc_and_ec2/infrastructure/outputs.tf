output "vpc_id" {
  value = aws_vpc.terraform-vpc.id
}

output "vpc_cidr_block" {
  value = aws_vpc.terraform-vpc.cidr_block
}

output "public_subnet_1_id" {
  value = aws_subnet.public-subnet-1.id
}

output "public_subnet_2_id" {
  value = aws_subnet.public-subnet-2.id
}

output "private_subnet_1_id" {
  value = aws_subnet.private-subnet-1.id
}

output "private_subnet_2_id" {
  value = aws_subnet.private-subnet-2.id
}

output "EC2_instance_private" {
  value = aws_instance.amazon-linux01.id
}

output "Elastic_load_balancer" {
  value = aws_elb.public_elb.dns_name
}

