#!/bin/bash
yum update -y
yum install httpd -y
service httpd start
chkconfig httpd on
export INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)
echo "<html><body><h1>Hello from EC2 instance <i>$INSTANCE_ID</i> created via Terraform</h1></body></html>" > /var/www/html/index.html
