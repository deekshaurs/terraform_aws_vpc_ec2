variable "region" {
  default     = "eu-central-1"
  description = "AWS Region"
}

variable "vpc_cidr" {
  default     = "10.0.0.0/16"
  description = "VPC CIDR Block"
}

variable "public_subnet_1_cidr" {
  description = "Public Subnet 1 CIDR"
}

variable "public_subnet_2_cidr" {
  description = "Public Subnet 2 CIDR"
}
variable "private_subnet_1_cidr" {
  description = "Private Subnet 1 CIDR"
}

variable "private_subnet_2_cidr" {
  description = "Private Subnet 2 CIDR"
}

variable "ec2_instance_type" {
  description = "EC2 instance type to be launched"
}

variable "key_pair_name" {
  default     = "VPC-EC2"
  description = "Key pair used to connect to EC2 instances"
}

variable "max_instance_size" {
  description = "Maximum number of autoscaling instances to launch"
}

variable "min_instance_size" {
  description = "Minimum number of autoscaling instances to launch"

}


