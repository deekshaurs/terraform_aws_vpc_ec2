provider "aws" {
  region  = var.region
  version = "~> 2.55"
}

terraform {
  backend "s3" {}
}

//creating VPC
resource "aws_vpc" "terraform-vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true

  tags = {
    Name = "Terraform-VPC"
  }
}

//security group for ec2 instances
resource "aws_security_group" "ec2_public_sg" {
  name        = "WebDMZ"
  description = "EC2 Security Group"

  vpc_id = aws_vpc.terraform-vpc.id
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
  }

  ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Terraform-EC2-SG"
  }
}

//creating public and private subnets
resource "aws_subnet" "public-subnet-1" {
  cidr_block        = var.public_subnet_1_cidr
  vpc_id            = aws_vpc.terraform-vpc.id
  availability_zone = "eu-central-1a"

  tags = {
    Name = "Public-Subnet-1"
  }
}

resource "aws_subnet" "private-subnet-1" {
  cidr_block        = var.private_subnet_1_cidr
  vpc_id            = aws_vpc.terraform-vpc.id
  availability_zone = "eu-central-1a"

  tags = {
    Name = "Private-Subnet-1"
  }
}

resource "aws_subnet" "public-subnet-2" {
  cidr_block        = var.public_subnet_2_cidr
  vpc_id            = aws_vpc.terraform-vpc.id
  availability_zone = "eu-central-1b"

  tags = {
    Name = "Public-Subnet-2"
  }
}

resource "aws_subnet" "private-subnet-2" {
  cidr_block        = var.private_subnet_2_cidr
  vpc_id            = aws_vpc.terraform-vpc.id
  availability_zone = "eu-central-1b"

  tags = {
    Name = "Private-Subnet-2"
  }
}

//creating route tables
resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.terraform-vpc.id

  tags = {
    Name = "Public-Route-Table"
  }
}

resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.terraform-vpc.id

  tags = {
    Name = "Private-Route-Table"
  }
}
//Associating subnets to route table
resource "aws_route_table_association" "public-subnet-1-association" {
  route_table_id = aws_route_table.public-route-table.id
  subnet_id      = aws_subnet.public-subnet-1.id
}

resource "aws_route_table_association" "public-subnet-2-association" {
  route_table_id = aws_route_table.public-route-table.id
  subnet_id      = aws_subnet.public-subnet-2.id
}

resource "aws_main_route_table_association" "main-route-table" {
  route_table_id = aws_route_table.private-route-table.id
  vpc_id         = aws_vpc.terraform-vpc.id
}


resource "aws_route_table_association" "private-subnet-1-association" {
  route_table_id = aws_route_table.private-route-table.id
  subnet_id      = aws_subnet.private-subnet-1.id
}

resource "aws_route_table_association" "private-subnet-2-association" {
  route_table_id = aws_route_table.private-route-table.id
  subnet_id      = aws_subnet.private-subnet-2.id
}

//creating elastic ip for NAT Gateway
resource "aws_eip" "elastic-ip-for-nat-gw" {
  vpc                       = true
  associate_with_private_ip = "10.0.0.5"

  tags = {
    Name = "Terraform-EIP"
  }
}

//creating NAT Gateway
resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.elastic-ip-for-nat-gw.id
  subnet_id     = aws_subnet.public-subnet-1.id

  tags = {
    Name = "Terraform-NAT-GW"
  }

}

//creating route out to internet for an instance in a private subnet using NAT Gateway
resource "aws_route" "nat-gw-route" {
  route_table_id         = aws_route_table.private-route-table.id
  nat_gateway_id         = aws_nat_gateway.nat-gw.id
  destination_cidr_block = "0.0.0.0/0"
}

//creating internet gateway
resource "aws_internet_gateway" "terraform-igw" {
  vpc_id = aws_vpc.terraform-vpc.id

  tags = {
    Name = "Terraform-IGW"
  }
}

//creating route out to internet using internet gateway
resource "aws_route" "public-internet-gw-route" {
  route_table_id         = aws_route_table.public-route-table.id
  gateway_id             = aws_internet_gateway.terraform-igw.id
  destination_cidr_block = "0.0.0.0/0"
}


//creating network access control list
resource "aws_network_acl" "nacl" {
  vpc_id     = aws_vpc.terraform-vpc.id
  subnet_ids = [aws_subnet.public-subnet-1.id, aws_subnet.private-subnet-1.id]

  tags = {
    name = "NACL_AZ1"
  }

}

//creating allow and deny rules
resource "aws_network_acl_rule" "nacl-tcp" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 100
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 0
  to_port        = 65535
}

resource "aws_network_acl_rule" "nacl-tcp1" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 100
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 0
  to_port        = 65535
}

resource "aws_network_acl_rule" "nacl-http" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 200
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 80
  to_port        = 80
}

resource "aws_network_acl_rule" "nacl-http1" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 200
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 80
  to_port        = 80
}


resource "aws_network_acl_rule" "nacl-ssh" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 300
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 22
  to_port        = 22
}

resource "aws_network_acl_rule" "nacl-ssh1" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 300
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 22
  to_port        = 22
}

resource "aws_network_acl_rule" "nacl_icmp" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 400
  egress         = false
  protocol       = "icmp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = -1
  to_port        = -1
  icmp_type      = -1
  icmp_code      = -1
}

resource "aws_network_acl_rule" "nacl_icmp1" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 400
  egress         = true
  protocol       = "icmp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = -1
  to_port        = -1
  icmp_type      = -1
  icmp_code      = -1
}

//Creating ec2 instance in private subnet
resource "aws_instance" "amazon-linux01" {
  ami                         = "ami-0ec1ba09723e5bfac"
  instance_type               = var.ec2_instance_type
  key_name                    = var.key_pair_name
  associate_public_ip_address = "false"
  security_groups             = ["${aws_security_group.ec2_public_sg.id}"]
  user_data                   = file("userdata_ec2.sh")



  root_block_device {
    volume_size           = "10"
    volume_type           = "standard"
    delete_on_termination = "true"
  }

  tags = {
    Name = "Private instance"
  }

  subnet_id = aws_subnet.private-subnet-1.id
}

//security group for Bastion host
resource "aws_security_group" "Bastion_sg" {
  name        = "Bastion Host"
  description = "Bastion Host Security Group"

  vpc_id = aws_vpc.terraform-vpc.id
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
  }

   egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Bastion-Host-SG"
  }
}

//Creating Bastion host in public subnet
resource "aws_instance" "amazon-Bastion" {
  ami                         = "ami-0ec1ba09723e5bfac"
  instance_type               = var.ec2_instance_type
  key_name                    = var.key_pair_name
  security_groups             = ["${aws_security_group.Bastion_sg.id}"]
  associate_public_ip_address = "true"



  root_block_device {
    volume_size           = "10"
    volume_type           = "standard"
    delete_on_termination = "true"
  }

  tags = {
    Name = "Bastion Host"
  }

  subnet_id = aws_subnet.public-subnet-1.id
}

//Security group for Elastic Load Balancer
resource "aws_security_group" "elb_sg" {
  name        = "ELB-SG"
  description = "ELB Security Group"
  vpc_id      = aws_vpc.terraform-vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow web traffic to load balancer"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Terraform-ELB-SG"
  }
}

// Creating a Classic Load Balancer for instances in Public Subnets 
resource "aws_elb" "public_elb" {
  name            = "WebServer-LoadBalancer"
  internal        = false
  security_groups = [aws_security_group.elb_sg.id]
  subnets         = [aws_subnet.public-subnet-1.id, aws_subnet.public-subnet-2.id]


  listener {
    instance_port     = 80
    instance_protocol = "HTTP"
    lb_port           = 80
    lb_protocol       = "HTTP"
  }

  health_check {
    healthy_threshold   = 3
    interval            = 5
    target              = "HTTP:80/index.html"
    timeout             = 4
    unhealthy_threshold = 2
  }

  tags = {
    Name = "Terraform-ELB"
  }

}

// Creating Launch Configuration for Auto Scaling group
resource "aws_launch_configuration" "ec2_public_launch_configuration" {
  image_id                    = "ami-0ec1ba09723e5bfac"
  instance_type               = var.ec2_instance_type
  key_name                    = var.key_pair_name
  security_groups             = [aws_security_group.ec2_public_sg.id]
  associate_public_ip_address = true

  user_data = file("userdata_ec2.sh")

  lifecycle {
    create_before_destroy = true
  }

}

// Creating Autoscaling Group
resource "aws_autoscaling_group" "ec2_public_autoscaling_group" {
  max_size             = var.max_instance_size
  min_size             = var.min_instance_size
  name                 = "EC2-Public-Autoscaling-Group"
  vpc_zone_identifier  = [aws_subnet.public-subnet-1.id, aws_subnet.public-subnet-2.id]
  launch_configuration = aws_launch_configuration.ec2_public_launch_configuration.id
  load_balancers       = [aws_elb.public_elb.id]
  health_check_type    = "ELB"

  tag {
    key                 = "Name"
    propagate_at_launch = false
    value               = "WebApp-EC2-Instance"
  }

}

// Creating Autoscaling policy to Scaleup or Scaledown the number of instances
resource "aws_autoscaling_policy" "ec2_webserver_autoscaling_policy" {
  autoscaling_group_name   = aws_autoscaling_group.ec2_public_autoscaling_group.name
  name                     = "Public-WebServer-AutoScaling-Policy"
  policy_type              = "TargetTrackingScaling"
  min_adjustment_magnitude = 1


  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 60.0
  }

}

resource "aws_autoscaling_policy" "increase-simple-scaling" {
  autoscaling_group_name = aws_autoscaling_group.ec2_public_autoscaling_group.name
  name                   = "ScaleUp-Increase"
  policy_type            = "SimpleScaling"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 10 // default = 300
}

resource "aws_autoscaling_policy" "decrease-simple-scaling" {
  autoscaling_group_name = aws_autoscaling_group.ec2_public_autoscaling_group.name
  name                   = "ScaleDown-Decrease"
  policy_type            = "SimpleScaling"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 10 // default = 300
}


